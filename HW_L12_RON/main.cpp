#include "MessagesSender.h"
#include <thread>

int main () 
{
	MessagesSender* m =  new MessagesSender();
	std::thread th(&MessagesSender::LogReg , m);
	std::thread thr(&MessagesSender::readData, m);
	std::thread thrr(&MessagesSender::writeData, m);
	th.join();
	thr.join();
	thrr.join();
}