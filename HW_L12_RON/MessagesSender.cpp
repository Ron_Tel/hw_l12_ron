#include "MessagesSender.h"
#include <fstream>
#include <string>
#include <thread>

//func showing the user options and ask him to choose from them
void MessagesSender :: LogReg () 
{
	int choise;
	std::string name;
	while (true) 
	{
		std::cout << "enter option :\n1. signin\n2. signout\n3. show connected users\n4. exit\n";
		std::cin >> choise;
		switch (choise)
		{
		case 1:
			std::cout << "enter your name : ";
			std::cin >> name;
			userMtx.lock();
			if (users.find(name) != users.end()) {
				std::cout << "user exist\n";
				break;
			}
			users.insert(name);
			userMtx.unlock();
			break;
		case 2:
			std::cout << "enter user name : ";
			std::cin >> name;
			userMtx.lock();
			if (users.find(name) == users.end()) {
				std::cout << "user dosent exist\n";
				break;
			}
			users.erase(name);
			userMtx.unlock();
			break;
		case 3:
			userMtx.lock();
			for (auto i : users) 
			{
				std::cout << i << "\n";
			}
			userMtx.unlock();
			break;
		case 4:
			return;
		default:
			break;

		}
	}
}

//func reading data from "data.txt" file once in 60 seconds and push him to messeges
void MessagesSender :: readData () 
{
	std::fstream file;
	std::string message;
	std::ofstream f;
	while (true) 
	{
		mesgMtx.lock();
		file.open("data.txt");
		while (getline(file, message))
			messages.push(message);
		file.close();
		mesgMtx.unlock();
		f.open("data.txt");
		f.close();
		std::this_thread::sleep_for(std::chrono::seconds(60));
	}
}

//func waiting for messeges to fill in and then write its contant to "output.txt"
void MessagesSender :: writeData () 
{
	std::fstream file;
	std::string msg;
	std::ofstream outfile("output.txt");
	while (true) 
	{
		while (messages.empty());
		while (!messages.empty())  
		{
			mesgMtx.lock();
			msg = messages.front();
			messages.pop();
			mesgMtx.unlock();
			userMtx.lock();
			for (auto user : users) 
			{
				file.open("output.txt" , std::ios::app);
				file <<"\n" << user << ": " << msg;
				file.close();
			}
			userMtx.unlock();
		}
	}
}